A próxima quarta-feira 07/09 é feriado nacional da Independência do Brasil e em função da data, o comércio terá o horário de funcionamento modificado para melhor atender os consumidores.  A Câmara de Dirigentes Lojistas de Natal  informa  como funcionarão os principais pontos comerciais da capital potiguar nesta quarta-feira.

### Comércio de Rua
Alecrim: Fechado  
Centro da Cidade: Fechado  
Zona Norte: Fechado



### Shoppings

#### Midway Mall:  
Praça de alimentação e Lazer: 11hs às 22hs  
Lojas de Departamento: de 13hs às 21hs  
Demais lojas: de 15hs às 21hs  


#### Natal Shopping:
Praça de Alimentação e Lazer: 11h às 22h.  
Lojas âncoras : 12 às 21h  
Mega Lojas: 13h às 21h  
Demais Lojas/ Quiosques: 14 às 21h  
Cinema conforme a programação

#### Praia Shopping:
Praça de Alimentação e Lazer: A partir das 11h.  
Lojas e Quiosques: 15 às 21h  
Cinema conforme a programação  

#### Shopping Cidade Jardim:
Praça de Alimentação: A partir das  11h  
Lojas e Quiosques: 15 às 21hs  

#### Shopping Via Direta:  

Praça de Alimentação e Lazer: 12h às 22h.  
Lojas e Quiosques: Abertura facultativa  


#### Partage Norte Shopping Natal
Lojas e quiosques: 15h às 21h  
Praça de Alimentação/Lazer: 11h às 22h  
Cinema conforme programação



### Supermercados
Funcionamento  das 07 às 22hs
