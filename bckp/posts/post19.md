A Câmara de Dirigentes Lojistas de Natal vai sortear 15 super prêmios entre os consumidores participantes da 15ª edição da Liquida Natal, que começa nesta sexta-feira 26/08 e segue até o dia 06/09. A abertura oficial da promoção acontece hoje, quarta-feira, às 18h30 no auditório do Hotel Holiday Inn, com a palestra de Bráulio Bessa, com o tema “Um jeito arretado de empreender”.

De acordo com o presidente da CDL Natal, Augusto Vaz, a ideia é levar os consumidores as compras na expectativa de concorrer aos prêmios da promoção, e assim movimentar o comércio. "Não é novidade para ninguém que o comércio local vem enfrentando quedas nas vendas, e nós como entidade representativa do setor estamos sempre buscando alternativas para reverter os números, e a Liquida Natal é uma grande oportunidade para isso. O consumidor gosta de concorrer a sorteios. O fato de saber que existe uma promoção no comércio por si só já motiva as pessoas a irem ao comércio", destacou.

Este ano a cada R$ 30,00 em compras o consumidor receberá um cupom para concorrer aos prêmios que são: 01 Fiat Toro, 07 TVs Led de 50”, 07 vales compras em supermercado no valor R$ de 500,00 durante um ano. Já os consultores de vendas concorrem a um vale compras no valor de R$ 1000,00.

#### Sobre a Liquida Natal

* Maior campanha do varejo na cidade e  segunda melhor data de vendas no ano.

* A promoção só é possível ser realizada devido a participação dos lojistas locais que adquirem os kits.

* O aplicativo [liquidanatal.com](http://liquidanatal.com) está de volta. Lojistas e consumidores terão na palma da mão as ofertas da campanha.
