A Câmara de Dirigentes Lojistas de Natal e a Prefeitura Municipal do
Natal entregam nesta sexta-feira, 04 de março, no Versailles Tirol,
durante a Assembleia Ordinária da Entidade, a premiação do Concurso de
Decoração Natalina 2015, promovido pela Prefeitura Municipal do Natal,
através da secretaria municipal de turismo e desenvolvimento econômico –
Setur em parceira com a Câmara de Dirigentes Lojistas de Natal – CDL
Natal.

O concurso foi realizado no período de 21 de novembro de 2015 a 06 de
janeiro de 2016, com o objetivo estimular a produção cultural,
iluminação decorativa e decorações criativas de vitrines e fachadas, em
empresas e residências situadas na cidade do Natal. Foram contempladas
cinco categorias, sendo elas: vitrines e fachadas de lojas; condomínios
comerciais e shoppings centers; hotéis, pousadas e restaurantes;
condomínios residenciais; e residências particulares.

Os premiados foram: Na categoria vitrines e fachadas a loja Aire; na
categoria condomínios comerciais e shopping centers, Shopping Cidade
Jardim; na categoria hotéis, pousadas e restaurantes, o Esmeralda Praia
Hotel; na categoria condomínios residenciais, o condomínio do Edifício
Vinícius de Moraes; e na categoria residências particulares, a Senhora
Luci Pereira Oliveira. Cada vencedor ganhou um troféu e cheque no valor
de R$ 5 mil reais cada.

Créditos da Foto Morais Neto


#### Imagens da premiação
![](img/posts/natalina2.jpg)
![](img/posts/natalina3.jpg)
![](img/posts/natalina4.jpg)
