Com a proposta de contribuir para melhorar as vendas do comércio, a Câmara de Dirigentes Lojista de Natal (CDL Natal) realiza na próxima terça-feira, 21/06, às 18h30, na sede da Entidade, a segunda edição da Terça do Conhecimento, com  “Como Transformar Clientes em Compradores”, que será ministrada pelo palestrante Emerson Alencar.

O tema da palestra será trabalhado de forma prática e participativa, abordando técnica de vendas com o objetivo de estabelecer relacionamento que favorece a venda, cria e comunicar o valor do produto, e despertar o “comprador” que existe em cada cliente.
