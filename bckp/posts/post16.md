Nos dias 12 e 13 de julho a CDL Natal realizou mais uma edição da Terça do Conhecimento, e contou com a parceria do Sebrae. Foram dois dias de palestras. Na terça-feira 13/07 os palestrantes Fred Rocha e João Kepler conversaram com os presentes sobre marketing digital, empreendedorismo, deram dicas aos empresários do varejo de como conquistar clientes e vender mais.

Alertaram que, em momentos de crise como a que o comércio vem passando, não se deve cortar os investimentos na empresa, e sim apresentar soluções para os problemas das pessoas e inovar, fazendo a mesma coisa, mas, de forma diferente.  “Na crise é preciso pensar além do óbvio, ser visionário, olhar além do tempo. Pessoas e negócios com atitudes similares obtêm resultados semelhantes. Por isso é importante se reinventar e inovar” João Kepler – Palestrante.

No segundo dia de palestras, os especialistas Fred Alecrim e Caio Camargo alertaram aos empresários sobre a necessidade de se adequar à nova realidade do mercado e não esperar o cenário desfavorável passar. “Quem está esperando aquela onda do mercado de dois ou três anos atrás, sinto informar, mas, essa onda não virá. O negócio terá de funcionar dentro do cenário de vendas atual", destacou Caio Camargo.

O segundo palestrante da noite foi o consultor potiguar Fred Alecrim que abordou as tendências e oportunidade de negócios que surgem em meio a cenários desfavoráveis. Ele deu dicas para vender mais mesmo durante a crise. “Esse é o momento para se escolher poucos indicadores. Se o empresário passa o tempo todo analisando indicadores, vai faltar tempo para pensar nos resultados. Para melhorá-los é preciso direção, velocidade e disciplina”, ressalta.

### Interiorização

As palestras da Terça do Conhecimento também foram assistidas por empresários do interior do Estado. No dia 12 o evento foi transmitido para 68 pessoas em Mossoró e 38 em Nova Cruz. Já no dia 13 transmitidas para as cidades de Assú, onde 54 participantes assistiram as palestras através de videoconferência, e de Pau dos Ferros, que teve 58 espectadores.
