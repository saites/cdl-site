O comércio vem enfrentando nos últimos meses sucessivas quedas nas vendas, 
empresas estabelecidas há anos no mercado tem encerrado as atividades, e cresce o número de desempregados no setor. 
Diante do cenário preocupante, a diretoria da Câmara de Dirigentes Lojistas de Natal tomou a iniciativa de convidar 
os lojistas para debater e se motivarem. Dai surgiu a Terça do Conhecimento, que acontecerá toda terceira terça-feira do mês na sede da CDL Natal.

O palestrante Jussier Ramalho é o primeiro convidado da CDL Natal para participar do projeto que 
acontece no próximo dia 17/05, às 18h30, na sede Entidade.  Com o tema "Mudança, ou Muda ou Dança” 
Jussier Ramalho vai abordar como o profissional precisa cuidar de sua carreira, construir uma marca, 
e ser referência no que faz, dentre outros pontos. O evento é voltado para lojistas e colaboradores.

Comunicação
Luciana Tito
99172-5275/ 98856-0532