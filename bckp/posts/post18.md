A CDL Natal está comemorando o sucesso dos workshops da Liquida Natal 2016. Sucesso de público, com a casa sempre cheia, as três primeiras noites de palestras para os consultores das empresas que aderiram a campanha contou com palestras motivacionais. Os palestrantes Gonçalo Pontes, Leandro Branquinho e Fred Alecrim deram uma verdadeira aula show de vendas.

A abertura oficial  da Liquida Natal acontece na próxima quarta-feira, no hotel Holiday  Inn, às 19hs, com a palestra do poeta empreendedor Bráulio Bessa.

A Liquida Natal é a maior promoção da cidade. É o segundo melhor período de vendas do comércio local, perdendo apenas para o Natal.
