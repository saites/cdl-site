A Semana Santa de 2016 é comemorada de 20 a 27 de Março, mas, somente o dia 25 é considerado feriado. A Câmara de Dirigentes Lojistas de Natal (CDL Natal) informa o funcionamento do comércio da capital potiguar na sexta-feira da paixão (25/03), quando os estabelecimentos funcionarão
com horários diferenciados.  No sábado 26/03 as atividades voltam ao  normal do horário comercial, e no domingo, os shoppings funcionam em  horário especial como de costume, praças de alimentação a partir das  11hs, lojas e quiosques das 13 às 21hs.

### Sexta-feira 25/03

#### Comércio de Rua
Alecrim: Lojas fechadas.

Centro da Cidade: Lojas fechadas

Zona Norte: Lojas fechadas.

#### Shopping Midway Mall
Praça de Alimentação e Lazer: 11h às 22h.

Lojas: A partir das 13h às 21h.

#### Natal Shopping
Praça de Alimentação e Lazer: 11h às 22h.

Lojas e Quiosques: 13h às 21h.

#### Praia Shopping
Praça de Alimentação e Lazer: A partir das 11h às 22h.

Lojas e Quiosques: 15h às 21h

#### Shopping Cidade Jardim
Praça de Alimentação: A partir das  11h

Lojas e Quiosques: 14 às 20h

#### Shopping Via Direta
Totalmente fechado

#### Partage Norte Shopping Natal
Praça de Alimentação e Lazer: 11 às 22h.

Lojas e Quiosques: 15 às 21h

#### Supermercados
Funcionamento das grandes redes das 07 às 22hs

#### Bancos
Fechados.

#### Comunicação
Luciana Tito  - Jornalista profissional

(84) 98856-0532/ 99175-5275

lucianatito@cdlnatal.com.br  

lucianatito@hotmail.com
