O Sebrae no Rio Grande do Norte, Sistema Fiern, Ministério do
Desenvolvimento, Indústria e Comércio Exterior (MDIC) e parceiros
apresentam, na próxima terça-feira (1), o Plano Nacional da Cultura
Exportadora (PNCE) a empresários potiguares. O plano visa aumentar a
quantidade de empresas com foco no mercado externo, diversificar as
exportações e ampliar a participação do país no comércio internacional.
A apresentação está marcada para começar às 8h30, no auditório do
Sebrae, em Natal.

O evento contará com a presença do coordenador nacional do PNCE, Eduardo
Weaver, e do supervisor de Competitividade da Agência Brasileira de
Promoção de Exportações e Investimentos (ApexBrasil), Tiago Terra. A
ideia é formar no estado um comitê gestor estadual para reunir
instituições que prestam apoio e alinhar as ações desenvolvidas para que
as pequenas empresas potiguares tenham o suporte necessário para chegar
ao mercado global.

Os interessados em participar do lançamento do plano no estado podem se
inscrever antecipadamente através da central de relacionamento do
Sebrae, através do 0800 570 0800, ou pela internet no Portal do Sebrae
(www.rn.sebrae.com.br/pnce/), onde está disponível o formulário de
inscrição.

Além do PNCE, também será apresentado ao empresariado potiguar durante o
evento o Projeto Extensão Industrial Exportadora (PEIEX), ligado à
ApexBrasil. O objetivo da iniciativa é estimular a competitividade e
promover a cultura exportadora nas empresas, qualificando e ampliando os
mercados para as indústrias iniciantes em Comércio Exterior.

O programa oferece às empresas um diagnóstico gratuito com o objetivo
de, posteriormente, no desenvolvimento do trabalho, apresentar soluções
a fim de impactar sobre o desempenho competitivo. A programação terá
palestra sobre a atuação do Ministério das Relações Exteriores no Apoio
às Exportações e outra sobre drawback. Ao final, haverá atendimento e
cadastramento das empresas interessadas em ingressar no PNCE.

O evento conta também com a parceria da Secretaria de Desenvolvimento
Econômico (Sedec), Ministério da Agricultura, Pecuária e Abastecimento
(MAPA), Banco do Brasil, Correios, Inframérica, Companhia Docas do RN
(Codern), Universidade Potiguar (UnP), Receita Federal e Ordem dos
Advogados do Brasil (OAB-RN).


Fonte: Portal NoarFonte: Portal Noar
