A Câmara de Dirigentes Lojistas de Natal – CDL Natal e o Banco do Nordeste do Brasil – BNB renovaram nesse mês de julho, durante reunião de diretoria da Entidade, o convênio de cooperação das linhas de crédito e investimentos no qual o BNB disponibilizará financiamentos em condições diferenciadas, para as empresas associadas da CDL Natal.  O Crédito pode ser utilizado para capital de giro, formação e renovação dos estoques, voltados para as principais datas comerciais do comércio, como Dia dos Pais, Liquida Natal, Dia das Crianças e as vendas de final de ano de 2016.



O presidente da CDL Natal augusto Vaz comemorou a renovação do convênio. “É uma satisfação para nossa Diretoria renovar esse convênio com o Banco do Nordeste. Nos últimos oito anos ele tem feito a diferença para nossos associados, e fortalecido o setor do comércio”, afirmou.



A principal linha de crédito deste convênio é a “Giro Estoque BNB”, porém o banco coloca a disposição dos associados da CDL Natal diversas linhas de crédito, de longo e de curto prazo, que podem financiar investimentos (construções civis, instalações, máquinas e equipamentos, informatização, etc) e capital de giro, em condições diferenciadas em relação às que são oferecidas pela rede bancária, que podem contribuir para aumentar a competitividade do empreendimento e a expansão dos negócios, transferindo os benefícios dessas linhas de crédito para os clientes finais
