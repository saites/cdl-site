A diretoria da Câmara de Dirigentes Lojistas de Natal (CDL Natal) recebeu na noite dessa quinta-feira 07/04, na sede da Entidade, o Comandante Geral do Corpo de Bombeiros Militar do RN, Coronel Otto Ricardo Saraiva de Souza para conversar com os lojistas sobre processo de renovação e licenciamento de imóveis.

O Coronel apresentou aos presentes como funcionam os processos de análise de projetos, a renovação e licenciamento de imóveis. Ele afirmou que atualmente esses trâmites estão demorando de 15 a 20 dias.  E as visitas em entre 30 e 40 dias.

O presidente da CDL Natal viu como positiva as informações repassadas pelo Coronel. “Em um momento de crise como o que estamos passando, saber que os processos para liberação de um imóvel comercial estão mais ágeis é muito otimista para o mercado, pois é comum em período de crises as pessoas decidirem empreender, e com essa informação tenho certeza que os empreendedores ficam mais animados a abrir um novo negócio”, afirmou o presidente.

Os empresários presentes interagiram e questionaram por diversas vezes o Coronel sobre o andamento dos processos dentro do Corpo de Bombeiros. Mas ao fim, ficam satisfeitos com as informações.  “Esses novos prazos apresentados pelo Coronel são positivos para comércio e para os lojistas. Antes, levávamos cerca de quatro meses para conseguir o alvará de funcionamento de um imóvel, o que dificultava a vida do empresário”, finalizou Augusto Vaz, presidente da CDL Natal.

![](img/posts/quinta2.jpg)

Crédito das Fotos: Luciana Tito
