O próximo domingo 1º de maio  é comemorado o Dia do Trabalhado, e em
função disso o comércio da capital potiguar volta a funcionar de forma
diferenciada. A Câmara de Dirigentes Lojistas de Natal (CDL Natal)
informa o funcionamento dos principais pontos comerciais durante o
feriado do dia do trabalho,  para que os consumidores possam se
programar e aproveirtar ao máximo o feriado.
É importante lembrar que o feriado do trabalhador  é protegido pela
convenção coletiva, em que o comércio fecha de maneira geral, inclusive
instituições financeiras e poder público, funcionando apenas as
atividades básicas como hospitais, alimentação e lazer.

Comércio de Rua: Fechado.

Shopping Midway Mall

* Lojas e quiosques: Fechados.
* Praça de Alimentação e Lazer: 11h às 22h.
* Cinema: Funciona na programação normal.

Natal Shopping

* Lojas e quiosques: Fechados.
* Praça de Alimentação e Lazer: 11h às 22h.


Partage Norte Shopping

* Lojas e quiosques: Fechados.
* Praça de Alimentação e Lazer: 11h às 22h.

Praia Shopping

* Lojas e quiosques: Fechados.
* Praça de Alimentação e Lazer: A partir das 11h.

Shopping Cidade Jardim

* Lojas e quiosques: Fechados.
* Praça de Alimentação: A partir das 11h.

Shopping Via Direta: Fechado.

Supermercados: Fechados.

Bancos: Fechados.
