var app = angular.module("CDL", ['ngRoute','vcRecaptcha','ngSanitize','btford.markdown']);

String.prototype.truncate =
  function(n){
      var p  = new RegExp("^.{0," + n + "}[\\S]*", 'g');
      var re = this.match(p);
      var l  = re[0].length;
      var re = re[0].replace(/\s$/,'');

      if (l < this.length) return re + '..';
  };

app.config(function($routeProvider) {
  var rp = $routeProvider;

  rp.when('/', {
    templateUrl: 'views/inicio.html'
  });

  rp.when('/institucional', {
    templateUrl: 'views/institucional.html'
  });


  rp.when('/galeria', {
    templateUrl: 'views/galeria.html'
  });

  rp.when('/contato', {
    templateUrl: 'views/contato.html',
    controller: 'FormContato'
  });

  rp.when('/servicos', {
    templateUrl: 'views/servicos.html'
  });

  rp.when('/associados', {
    templateUrl: 'views/associados.html'
  });

  rp.when('/boleto-online', {
    templateUrl: 'views/boleto-online.html'
  });

  rp.when('/noticias', {
    templateUrl: 'views/blog.html',
    controller: 'NoticiaCtrl'
  });

  rp.when('/post/:id', {
    templateUrl: 'views/post.html',
    controller:"PostCtrl"
  });

  rp.when('/associa-se', {
    templateUrl: 'views/associase-contato.html',
    controller: 'FormAsso'
  });

  rp.when('/auditorios', {
    templateUrl: 'views/auditorios.html'
  });

  rp.when('/terca-do-conhecimento', {
    templateUrl: 'views/terca_do_conhecimento.html',
    controller: 'FormTerca'
  });

  rp.when('/posnfr', {
    templateUrl: 'views/posnfr.html',
    controller: 'FormPosNfr'
  });

  rp.when('/evento', {
    templateUrl: 'views/evento.html',
    controller: 'FormEvento'
  });

  rp.when('/decoracao-natalina-2018', {
    templateUrl: 'views/decoracao_natalina_2017.html',
    controller: 'FormNatalina2017'
  });


  rp.when('/concilia2018', {
    templateUrl: 'views/concilia2018.html',
    controller: 'FormConcilia2018'
  });

  
 



  rp.otherwise({
    redirectTo: '/'
  });

});

function viewport() {
  var e = window, a = 'inner';
  if (!('innerWidth' in window )) {
      a = 'client';
      e = document.documentElement || document.body;
  }
  return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}

app.run(function($rootScope){



  $rootScope.tab = '';


  $rootScope.$on( "$routeChangeStart", function(event, next, current) {
     console.log(next.$$route.templateUrl);

     switch (next.$$route.templateUrl) {
       case 'views/servicos.html':
         $rootScope.tab = 'serv'
         break;
       case 'views/associados.html':
         $rootScope.tab = 'asso'
         break;
       case 'views/galeria.html':
         $rootScope.tab = 'galeria'
         break;
       default:
         $rootScope.tab = '';
     }
   });

});



app.controller("SlideCtrl",function($scope, $location, $window){
  $scope.action = null;

  if(viewport().width > 800){
    $scope.slides = [
      "img/decoracao-natalina-2018.jpg",
      "img/concilia2018.png",
      "img/banner_seguranca.jpg",
    ];
  } else {
    $scope.slides = [
     "img/decoracao-natalina-2018p.jpg",
     "img/concilia2018p.png",
     "img/banner_segurancaM.jpg",
    ];
  }

  $scope.slide = {}; // slide atual
  $scope.slide.index = 0; // index do slide atual

  function safeApply(fn) {
    ($scope.$$phase || $scope.$root.$$phase) ? fn() : $scope.$apply(fn);
  }

  var timeChange = null; // ultima chamada para changeSlide();
  var timeClass  = null; // ultima chamada para fadeout

  function changeSlide(){
    // Definindo o slide atual
    safeApply(function(){
      $scope.efeito = "inslide";
      $scope.slide.image = $scope.slides[$scope.slide.index];


      switch($scope.slide.index) {
	   case 0:
  
        $scope.action = function(){
          
          window.location.replace('#/decoracao-natalina-2018');
        };
        break;      	
      	/*
		   case 0:
     
            $scope.action = function(){
              
              window.location.replace('#/concilia2018');
            };
            break;
            
		   case 1:
      
            $scope.action = function(){
              
              window.open('http://cdlnatal.com.br/arquivos/termo-autorização.pdf');
            };
            break;
        */
        default:
            $scope.action = function(){};

      }

    });

    var time = 10000;
    // animação de saida
    timeClass = setTimeout(function(){
      safeApply(function(){
        $scope.efeito = "outslide";
      });
    },(time - 1000));
    // chamando a função para ficar alterando os slides infinatamente.
    timeChange = setTimeout(function () {
      // incrementando
      $scope.slide.index ++;
      if($scope.slide.index == $scope.slides.length) $scope.slide.index = 0;
      changeSlide();
    }, time);
  }

  $scope.nextSlide = function(){
    clearTimeout(timeChange);
    clearTimeout(timeClass);
    $scope.slide.index ++;
    if($scope.slide.index == $scope.slides.length) $scope.slide.index = 0;
    changeSlide();
  };

  $scope.setSlide = function(index){
    clearTimeout(timeChange);
    clearTimeout(timeClass);
    $scope.slide.index = index;
    changeSlide();
  };

  $scope.prevSlide = function(){
    $scope.slide.index -= 1;
    if($scope.slide.index < 0 ) $scope.slide.index = $scope.slides.length - 1;

    clearTimeout(timeChange);
    clearTimeout(timeClass);
    changeSlide();
  };

  // chamando pela primeira vez
  changeSlide();
});

app.controller("FormContato", function($scope, $http, Email){

  $scope.data = {};
  $scope.captcha = null;


  $scope.submit = function(){
    if ($scope.captcha == null) { alert("Prove que não é um robô com o captcha."); return; }
    if ($scope.data.setor == undefined) { alert("Setor é obrigatório!"); return; }
    if ($scope.data.nome == undefined) { alert("Nome é obrigatório!"); return; }
    if ($scope.data.email == undefined) { alert("Email é obrigatório!"); return; }
    if ($scope.data.telefone == undefined) { alert("Telefone é obrigatório!"); return; }

    Email.contato($scope.data).then(function(){
      alert("Formulário enviado com sucesso!");
      $scope.data = {};
    });
  };

});

app.controller("FormAsso", function($scope, $http, Email){

  $scope.data = {};
  $scope.captcha = null;

  $scope.submit = function(){
    if ($scope.captcha == null) { alert("Prove que não é um robô com o captcha."); return; }
    if ($scope.data.telefone == undefined) { alert("Telefone é obrigatório!"); return; }
    if ($scope.data.nome == undefined) { alert("Nome é obrigatório!"); return; }
    if ($scope.data.email == undefined) { alert("Email é obrigatório!"); return; }
    if ($scope.data.cnpj == undefined) { alert("CNPJ é obrigatório!"); return; }
    if ($scope.data.razao_social == undefined) { alert("Razão social é obrigatório!"); return; }

    $scope.data.mensagem = $scope.data.mensagem || "" ;

    Email.associase($scope.data).then(function(){
      alert("Formulário enviado com sucesso!");
      $scope.data = {};

    });
  };

});

app.controller("FormTerca", function($scope, $http, Email){

  $scope.data = {};
  $scope.captcha = null;

  $scope.submit = function(){
    if ($scope.captcha == null) { alert("Prove que não é um robô com o captcha."); return; }
    if ($scope.data.nome == undefined) { alert("Nome é obrigatório!"); return; }
    //if ($scope.data.cpf == undefined) { alert("CPF é obrigatório!"); return; }
    if ($scope.data.empresa == undefined) { alert("Nome da empresa é obrigatório!"); return; }
    if ($scope.data.cnpj == undefined) { alert("CNPJ é obrigatório!"); return; }
    if ($scope.data.email == undefined) { alert("Email é obrigatório!"); return; }
    if ($scope.data.telefone == undefined) { alert("Telefone é obrigatório!"); return; }

    Email.terca($scope.data).then(function(){
      alert("Formulário enviado com sucesso!");
      $scope.data = {};

    });
  };

});


app.controller("FormPosNfr", function($scope, $http, Email){

  $scope.data = {};
  $scope.captcha = null;

  $scope.submit = function(){
    if ($scope.captcha == null) { alert("Prove que não é um robô com o captcha."); return; }
    if ($scope.data.nome == undefined) { alert("Nome é obrigatório!"); return; }
    if ($scope.data.empresa == undefined) { alert("Nome da empresa é obrigatório!"); return; }
    if ($scope.data.email == undefined) { alert("E-mail é obrigatório!"); return; }
    if ($scope.data.telefone == undefined) { alert("Telefone é obrigatório!"); return; }

    Email.pos($scope.data).then(function(){
      alert("Formulário enviado com sucesso!");
      $scope.data = {};

    });
  };

});


app.controller("FormNatalina2017", function($scope, $http, Email){

  $scope.data = {};
  $scope.captcha = null;

  $scope.submit = function(){
    if ($scope.captcha == null) { alert("Prove que não é um robô com o captcha."); return; }
    if ($scope.data.nome == undefined) { alert("Nome é obrigatório!"); return; }

    if ($scope.data.empresa == undefined) { alert("Nome da empresa é obrigatório!"); return; }
    if ($scope.data.funcao == undefined) { alert("função é obrigatório!"); return; }
	if ($scope.data.endereco == undefined) { alert("Endereço é obrigatório!"); return; }
    

    //if ($scope.data.cpf == undefined) { alert("CPF é obrigatório!"); return; }
    
    
    if ($scope.data.categoria == undefined) { alert("Categoria é obrigatório!"); return; }
    if ($scope.data.email == undefined) { alert("Email é obrigatório!"); return; }

    
    if ($scope.data.telefone == undefined) { alert("Telefone é obrigatório!"); return; }

    Email.natalino($scope.data).then(function(){
      alert("Formulário enviado com sucesso!");
      $scope.data = {};
    });
  };

});


app.controller("FormConcilia2018", function($scope, Email){

  $scope.data = {};
  $scope.captcha = null;

  $scope.submit = function(){
 
    if ($scope.captcha == null) { alert("Prove que não é um robô com o captcha."); return; }
    if ($scope.data.nome == undefined) { alert("Nome é obrigatório!"); return; }

    if ($scope.data.telefone == undefined) { alert("Telefone é obrigatório!"); return; }
    if ($scope.data.responsavel == undefined) { alert("Responsável é obrigatório!"); return; }
    if ($scope.data.cpf == undefined) { alert("CPF é obrigatório!"); return; }
    
    
    Email.spc($scope.data).then(function(){
      alert("Formulário enviado com sucesso!");
      $scope.data = {};
    });
  };
  

});

app.controller("FormEvento", function($scope, $http, Email){

  $scope.data = {};
  $scope.captcha = null;

  $scope.submit = function(){
    if ($scope.captcha == null) { alert("Prove que não é um robô com o captcha."); return; }
    if ($scope.data.nome == undefined) { alert("Nome é obrigatório!"); return; }
    //if ($scope.data.cpf == undefined) { alert("CPF é obrigatório!"); return; }
    if ($scope.data.funcao == undefined) { alert("função é obrigatório!"); return; }
    if ($scope.data.endereco == undefined) { alert("Endereço é obrigatório!"); return; }
    if ($scope.data.categoria == undefined) { alert("Categoria é obrigatório!"); return; }
    if ($scope.data.empresa == undefined) { alert("Nome da empresa é obrigatório!"); return; }
    if ($scope.data.cnpj == undefined) { alert("CNPJ é obrigatório!"); return; }
    if ($scope.data.email == undefined) { alert("Email é obrigatório!"); return; }
    if ($scope.data.telefone == undefined) { alert("Telefone é obrigatório!"); return; }

    Email.natalino($scope.data).then(function(){
      alert("Formulário enviado com sucesso!");
      $scope.data = {};
    });
  };

});
app.controller("newsletter", function($scope, Email){

  $scope.data = {};

  $scope.submit = function(){
    if ($scope.data.email == undefined) { alert("Email é obrigatório!"); return; }

    Email.news($scope.data.email).then(function(){
      alert("Email enviado com sucesso!");
      $scope.data = {};
    });
  };

});


app.controller("galeria", function($scope, $http){

  $scope.images = [
    'img/galeria01.jpeg',
    'img/galeria02.jpeg',
    'img/galeria03.jpeg',
    'img/galeria04.jpeg',
    'img/galeria05.jpeg',
    'img/galeria06.jpeg'
  ];

  $scope.show = function(url){
    $scope.target = url;
  };

  $scope.hide = function(){
    $scope.target = null;
  };

});

app.controller("PostCtrl", function($scope,$routeParams, $http , $window){
  $scope.post = {};
  var id = $routeParams.id;
  if(id){
    $http.get("/admin/post.php?id="+id).then(function(res){
      $scope.post = res.data[0];
      console.log($scope.post);
    });
  } else {
    $window.location = "#/noticias";
  }
});


app.controller("NoticiaCtrl", function($scope, $http){
  $scope.posts = [];

  $http.get("/admin/posts.php").then(function(res){
    for (var i = 0; i < res.data.length; i++) {
      res.data[i].index = i;
    }
    $scope.posts = res.data;
  });

});


app.controller("LastNew", function($scope, $http){
  $scope.posts = [];

  $http.get("/admin/last_news.php").then(function(res){

    $scope.posts = res.data;

    console.log($scope.posts);
  });

});


app.factory("Email", function($http, $q){

  return {
    natalino: function(data){
      var q = $q.defer();

      var params =
        "?nome="     + data.nome +
        "&natalino="      + 'true' +
        "&empresa="  + data.empresa +
		"&funcao="  + data.funcao +
		"&endereco="  + data.endereco +
        "&cpf="      + data.cpf +
        "&categoria="  + data.categoria +
        "&email="    + data.email +
        "&fone=" + data.telefone;

        console.log(params)

      $http.get('http://cdlnatal.com.br/email/evento.php' + params).then(function(){
         q.resolve();
      }, function(){
         q.reject();
      });

      return q.promise;
    },
    terca: function(data){
      var q = $q.defer();

      var params =
        "?nome="     + data.nome +
        "&cpf="      + data.cpf +
        "&empresa="  + data.empresa +
        "&cnpj="     + data.cnpj +
        "&email="    + data.email +
        "&fone=" + data.telefone;

      $http.get('http://cdlnatal.com.br/email/evento.php' + params).then(function(){
         q.resolve();
      }, function(){
         q.reject();
      });

      return q.promise;
    },
   pos: function(data){
      var q = $q.defer();


      var params =
        "?nome="     + data.nome +
        "&empresa="  + data.empresa +
        "&email="  + data.email +
        "&telefone="  + data.telefone;
       

      $http.get('http://cdlnatal.com.br/email/pos.php' + params).then(function(){

         q.resolve();
      }, function(){
         q.reject();
      });

      return q.promise;
    },
    associase: function(data){
      var q = $q.defer();

      var params =
        "?nome="     + data.nome  +
        "&email="    + data.email +
        "&fone=" + data.telefone +
        "&cnpj="     + data.cnpj  +
        "&razao_social=" + data.razao_social +
        "&mensagem=" +  data.mensagem ;


      $http.get('http://cdlnatal.com.br/email/associado.php' + params).then(function(){
         q.resolve();
      }, function(){
         q.reject();
      });

        return q.promise;
    },
    news: function(email) {
      var q = $q.defer();
      var params ="?email=" + email ;

      $http.get('http://cdlnatal.com.br/email/news.php' + params).then(function(){
         q.resolve();
      }, function(){
         q.reject();
      });

      return q.promise;
    },
    contato: function(data){
      var q = $q.defer();

      var params =
        "?nome="     + data.nome  +
        "&email="    + data.email +
        "&assunto=" + data.assunto +
        "&setor="     + data.setor  +
        "&telefone="     + data.telefone  +
        "&mensagem=" +  data.mensagem ;


      $http.get('http://cdlnatal.com.br/email/contato.php' + params).then(function(){
         q.resolve();
      }, function(){
         q.reject();
      });

        return q.promise;
    },spc: function(data){
      var q = $q.defer();


      var params =
        "?nome="     + data.nome +
        "&telefone="  + data.telefone +
        "&responsavel="  + data.responsavel +
        "&cpf="  + data.cpf;
       

      $http.get('http://cdlnatal.com.br/email/spc.php' + params).then(function(){

         q.resolve();
      }, function(){
         q.reject();
      });

      return q.promise;
    },
  };
});
