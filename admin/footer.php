</div>
</div>

<footer>
<div class="content">

    <ul>
      <li><i class="fa fa-map-marker"></i> Rua Ceará Mirim 322 - Tirol, 59020-240 - Natal-RN</li>
      <li><i class="fa fa-phone"></i> Telefone: (84) 4009-0000</li>
      <li><i class="fa fa-envelope"></i> E-mail: <a href="mailto:atendimento@cdlnatal.com.br">atendimento@cdlnatal.com.br</a></li>

    </ul>

    <p class="social">
      Fique por dentro das novidades da CDL Natal nas redes sociais: <br>
      <a href="https://m.facebook.com/cdlnat/" target="_blank"><i class="fa fa-facebook"></i></a>
      <a href="https://mobile.twitter.com/CDLNatal" target="_blank"><i class="fa fa-twitter"></i></a>
      <a href="https://www.youtube.com/channel/UCOfM7zRd0rXQK_ibXqKZPig" target="_blank"><i class="fa fa-youtube"></i></a>
    </p>

  <a href="http://www.rejsolucoes.com.br/" target="_blank"><img class="rej" src="../img/rej.png" alt="" /></a>
</div>
</footer>

</body>
</html>
