<?php

header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: X-Requested-With, X-authentication, X-client');
include('database.php');

$id = $_GET['id'];

$stmt = getConn()->query(
  "SELECT * FROM posts WHERE id = {$id};"
);
// "SELECT * FROM posts ORDER BY id DESC LIMIT 10 OFFSET {$offset};"

$list = $stmt->fetchAll(PDO::FETCH_OBJ);

echo json_encode($list);
