<!DOCTYPE html>
<html>
<head>
	<title>CDL</title>
	<meta charset='utf-8'>
  <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/media-querys.css">

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:800,400,600' rel='stylesheet' type='text/css'>

	<script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>
	<script src="../vendor/simplemde/dist/simplemde.min.js"></script>

	<script src="../vendor/moment/moment.js"></script>

	<script src="../js/pikaday.js"></script>
	<link rel="stylesheet" href="/css/pikaday.css">

	<link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.0/jquery.js"></script>

</head>
<body ng-app="CDL">
	<header>
    <div class="blue-dark">
      <div class="content">
         <ul>
           <a href="/#/"><li>CDL WEBSITE</li></a>
					 <?php if((!isset ($_SESSION['login']) != true) || (!isset ($_SESSION['senha']) != true)){ ?>
           <a href="/admin/index.php"><li>POSTS</li></a>
           <a href="/admin/novo_post.php"><li>CRIAR UM NOVO POST</li></a>
					 <a href="/admin/sair.php"><li>SAIR</li></a>
					 <?php  } ?>
         </ul>


      </div>
    </div>

		<div class="content">
			<div class="col-22 logo">
				<a href="/#/"><img src="../img/logo.png" alt="Logo da CDL." /></a>
			</div>
	</header>


<div class="view">
	<div autoscroll="true">
