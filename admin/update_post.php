<?php
include('database.php');

$title = $_POST['title'];
$description = $_POST['description'];
$auth= $_POST['auth'];
$text= $_POST['text'];
$id = $_POST['id'];

$uploaddir = '../public/uploads/';
$uploadfile = $uploaddir . basename($_FILES['image']['name']);

if ($_FILES['image']['size'] != 0){
  if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
      $image = $uploadfile;
      $sql =
      "UPDATE posts SET title = :title, image = :image, description = :description, auth = :auth, text = :text WHERE id = :id";
  } else {
      echo "Possível ataque de upload de arquivo!\n";
      return false;
  }
} else {
  $sql =
  "UPDATE posts SET title = :title, description = :description , auth = :auth, text = :text WHERE id = :id";
}




$conn = getConn();
$stmt = $conn->prepare($sql);

$stmt->bindParam("id",$id);
$stmt->bindParam("title",$title);

if ($_FILES['image']['size'] != 0){
  $stmt->bindParam("image",$image);
}

$stmt->bindParam("description",$description);
$stmt->bindParam("auth",$auth);
$stmt->bindParam("text",$text);

$sucess = $stmt->execute();

if($sucess == 1) {
  header('Location: /admin/index.php');
} else {
  header('Location: /admin/novo_post.php');
}
