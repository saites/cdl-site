<?php
include('database.php');

$id = $_GET['id'];

$sql = "DELETE FROM posts WHERE id = :id;";
$conn = getConn();
$stmt = $conn->prepare($sql);

$stmt->bindParam("id", $id);
$sucess = $stmt->execute();

header('Location: /admin/index.php');
