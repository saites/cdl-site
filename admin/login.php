<?php include ('header.php'); ?>

<style media="screen">
  form {
    padding: 0px 20px 20px 20px;
    border: 1px solid rgba(0,0,0,0.1);
    margin-top: 20px;
  }

  .error {
    color: red;
  }
</style>

<div class="content" style="min-height: 60vh !important;">
  <form  action="/admin/auth.php" method="POST">

    <h1 class="title-blue">Login</h1>
    <?php if ($_GET['falha'] != null) {?>
    <p class="error">
      Problemas com o email ou a senha.
    </p>
    <?php }  ?>
    <label>E-mail</label>
    <input type="email" required name="email">

    <label>Senha</label>
    <input type="password" required name="password">

    <button type="submit" name="button">ENTRAR</button>
  </form>

  <br><br>

</div>


<?php include ('footer.php'); ?>
