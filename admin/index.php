<?php

include('check_login.php');
include ('header.php');
include('database.php');


$page = $_GET['page'];
if(!$page){ $page = 1; }

$page = $page - 1;
$offset = $page * 10;

$stmt = getConn()->query(
  "SELECT * FROM posts ORDER BY id DESC LIMIT 10 OFFSET {$offset};"
);

$list = $stmt->fetchAll(PDO::FETCH_OBJ);


$stmt = getConn()->prepare("SELECT COUNT(*) AS count FROM posts");
$stmt->execute();
$result = $stmt->fetch();
?>


<style>
  .image-post {
    background-size:cover;
    background-repeat:no-repeat;
    width:80px;
    height:80px;
    margin-top: 30px;
    margin-right: 20px;
  }

  .post {
    border-bottom: 1px solid rgba(0,0,0,0.1);
  }

  .side {
    display: inline-block;
    vertical-align: top;
  }
</style>


<div class="content" style="min-height: 60vh !important;">

  <?php foreach ( $list as &$post) {?>
    <div class="post">
      <a class="side" href="/#/post/<?php echo $post->id  ?>">
        <div  class="image-post" style="background-image: url('<?php echo $post->image  ?>');"></div>
      </a>

      <div class="side">


          <a href="/#/post/<?php echo $post->id  ?>"><h4 class="title"><?php echo $post->title  ?></h4></a>
          <p class="no-margin-top"><?php echo $post->date  ?> | Postado por <?php echo $post->auth  ?> </p>

          <br><a href="/admin/editar.php?id=<?php echo $post->id  ?>">Editar post</a> |
          <a href="/admin/remover.php?id=<?php echo $post->id  ?>" onclick="return confirm('Você tem certeza?')">Excluir post</a>
          <p class="no-margin-top"> <?php echo $post->description  ?></p>

      </div>

    </div>
  <?php } ?>

   <ul class="pages">
     <?php  $pages =  round(($result['count'] / 10) + 0.5)?>
     <li>Página 1 de <?php echo $pages ?></li>
     <?php if ($page > 0) {  echo '<a href="/admin/index.php?page='. ($page) .'"><li>< anterior</li></a>'; } ?>

     <?php for($i = 1 ; $i <= $pages; $i++ ){
       echo '<a href="/admin/index.php?page='.$i.'"><li>'.$i.'</li></a>';
       }
     ?>
     <?php if ($page < ($pages -1 )) { echo '<a href="/admin/index.php?page='. ($page + 2) .'"> <li>próximo ></li> </a>'; } ?>
   </ul>
   <br>

</div>


<?php include ('footer.php'); ?>
