<?php
include('database.php');

$title = $_POST['title'];
$description = $_POST['description'];
date_default_timezone_set('America/Sao_Paulo');

$date= $_POST['date'];
//$date= date_create()->format('Y-m-d');

$auth= $_POST['auth'];
$text= $_POST['text'];

$uploaddir = '../public/uploads/';
$uploadfile = $uploaddir . basename($_FILES['image']['name']);

if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
    $image = $uploadfile;
} else {
    echo "Possível ataque de upload de arquivo!\n";
    return false;
}


$sql =
"INSERT INTO posts (title, image, description, date, auth, text) VALUES (" .
" :title, " .
" :image, " .
" :description, " .
" :date, " .
" :auth, " .
" :text " .
");";

$conn = getConn();
$stmt = $conn->prepare($sql);

$stmt->bindParam("title",$title);
$stmt->bindParam("image",$image);
$stmt->bindParam("description",$description);
$stmt->bindParam("date",$date);
$stmt->bindParam("auth",$auth);
$stmt->bindParam("text",$text);

$sucess = $stmt->execute();

if($sucess == 1) {
  header('Location: /admin/index.php');
} else {
  header('Location: /admin/novo_post.php');
}
