<?php
include('check_login.php');
include ('header.php');
include('database.php');


$id = $_GET['id'];

$stmt = getConn()->prepare("SELECT *  FROM posts WHERE id = {$id}");
$stmt->execute();
$post = $stmt->fetch();
?>

<style>
  .img-editar {
    max-width: 200px;
  }
</style>

<div class="content" style="min-height: 60vh !important;">
  <form enctype="multipart/form-data" action="/admin/update_post.php" method="POST">
    <h1 class="title-blue">Editar Post</h1>
    <p>Todos os campos são obrigatórios.</p>
    <input type="hidden" name="id" value="<?php echo $post['id'] ?>">
    <label>Título da matéria</label>
    <input type="text" name="title" required value="<?php echo $post['title'] ?>">


    <label>Imagem do post</label>
    <img class="img-editar" src="<?php echo $post['image'] ?>" alt="" />
    <input type="file" name="image">

    <label>Descrição</label>
    <p>Curta decrição da matéria.</p>
    <input type="text" name="description" required value="<?php echo $post['description'] ?>">

    <label>Autor(a)</label>
    <input type="text" name="auth" required value="<?php echo $post['auth'] ?>">

    <label>Matéria do post</label>
    <div>
      <textarea name="text"  spellcheck="false" rows="8" cols="40"><?php echo $post['text'] ?></textarea>
    </div>


    <button type="submit" name="button">ENVIAR</button>
  </form>

  <br><br>

</div>

<script>
var simplemde = new SimpleMDE({ spellChecker: false });
</script>

<?php include ('footer.php'); ?>
