<?php

header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: X-Requested-With, X-authentication, X-client');
include('database.php');

$page = $_GET['page'];
if(!$page){ $page = 1; }

$page = $page - 1;
$offset = $page * 10;

$stmt = getConn()->query(
  "SELECT * FROM posts ORDER BY id DESC;"
);
// "SELECT * FROM posts ORDER BY id DESC LIMIT 10 OFFSET {$offset};"

$list = $stmt->fetchAll(PDO::FETCH_OBJ);

echo json_encode($list);
