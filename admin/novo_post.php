<?php include('check_login.php'); ?>
<?php include ('header.php'); ?>


<div class="content" style="min-height: 60vh !important;">
  <form  enctype="multipart/form-data" action="/admin/create_post.php" method="POST">
    <h1 class="title-blue">Novo Post</h1>
    <p>Todos os campos são obrigatórios.</p>
    <label>Título da matéria</label>
    <input type="text" name="title" required>

    <label>Imagem do post</label>
    <input type="file" name="image" required>

    <label>Descrição</label>
    <p>Curta decrição da matéria.</p>
    <input type="text" name="description" required>

    <label>Data</label>
    <input id="datepicker" type="text" name="date" required>

    <label>Autor(a)</label>
    <select name="auth" required>
      <option value="Luciana">Luciana</option>
    </select>

    <label>Matéria do post</label>
    <div>
      <textarea name="text"  spellcheck="false" rows="8" cols="40"></textarea>
    </div>


    <button type="submit" name="button">ENVIAR</button>
  </form>

  <br><br>

</div>

<script>
  var simplemde = new SimpleMDE({ spellChecker: false, required: true });
  var picker = new Pikaday({
    field: document.getElementById('datepicker'),
    i18n: {
        previousMonth : 'Mês anterior',
        nextMonth     : 'Próximo mês',
        months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
    }
  });
</script>

<?php include ('footer.php'); ?>
