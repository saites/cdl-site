Mês pode ser considerado um dos piores da história em termos de
desemprego no comércio, aponta CDL-Natal.

O Cadastro Geral de Empregados e Desempregados (Caged) mostrou que quase
três mil potiguares perderam seus empregos em janeiro deste ano. Segundo
o relatório mensal, 2.944 postos de trabalho foram perdidos no primeiro
mês de 2016, número que representa “uma redução de 0,66% em relação ao
estoque de assalariados com carteira assinada do mês anterior”, como
aponta o documento.

A atividade que mais contribuiu para esse dado foi o Comércio, seguida
de perto da Agropecuária. Fevereiro começou com 1.255 menos empregados
no setor comerciário. Segundo o presidente da Câmara de Dirigentes
Lojistas de Natal (CDL-Natal), Augusto Vaz, janeiro costuma ser um mês
onde há demissões porque é o período posterior às festas de fim de ano.

Porém, Vaz disse que não dá para creditar o índice de desemprego a isso.
“Quase não teve emprego temporário no final do ano passado, então nem
para para culpar só os temporários. Os números refletem o momento da
economia e o comércio vem perdendo força com fechamentos de empresas e
redução das equipes”, disse.

De acordo com o representante da CDL, esse “pode ser considerado um dos
piores janeiros da história em números de demissões” e a tendência não é
melhorar. “Possivelmente os números serão piores ainda nos próximos
meses devido à nossa realidade econômica e as projeções”, lamentou
Augusto Vaz.

Agropecuária

O panorama no agronegócio também não é o dos melhores. O setor demitiu
1.053 pessoas em janeiro, número que pode ser explicado, segundo o
presidente da Federação da Agricultura e Pecuária do RN (Faern), José Álvares Vieira, pelo fim da safra de cana de açúcar. O mês em questão
historicamente tem desligamentos de postos de trabalho no setor rural.

Mas, como ocorre na cidade com o comércio, os produtores não estão
animados para o restante de 2016. “Não tenha dúvida que o setor vem
piorando e as perspectivas não são nada boas para o resto do ano”,
adiantou Vieira. O motivo para as previsões negativas é, além do atual
cenário econômico do país, a seca prolongada no interior, situação que
atinge em cheio áreas como a fruticultura.

Fonte: Portal Noar

Repórter: Felipe Galdino

Foto: Alberto LeandroFoto: Alberto Leandro
