Que as redes sociais e a mídia são utilizadas para diversos fins de consumo já é um fato amplamente conhecido. Porém, mostrar como especificamente as mulheres se comportam e respondem à influências de compra e avaliar a percepção da sua imagem utilizada pelos meios de comunicação é o objetivo da série de pesquisas “O Perfil de Consumo das Mulheres Brasileiras” do Serviço de Proteção ao Crédito (SPC Brasil) e da Confederação Nacional de Dirigentes Lojistas (CNDL). Segundo o estudo, 64,8% das brasileiras entrevistadas admitem que já mudaram seus hábitos de compra por causa das redes sociais. Ou seja, o fato de acompanhar posts, dicas e comentários teve algum efeito sobre o cotidiano e comportamento de consumo dessa mulher, fazendo com que ela passasse a comprar produtos sugeridos ou mudasse algumas prática do dia a dia.

De acordo com os dados obtidos, as redes sociais são utilizadas por 94,3% das entrevistadas. A plataforma mais popular é o Facebook, citado por 89,2% das mulheres entrevistadas, seguido pelo Youtube (43,4%) e pelo Instagram (34,4%). A pesquisa também abordou os temas que mais mobilizam as postagens, compartilhamentos e acompanhamento das mulheres nas redes sociais: culinária (65,1%), moda (46,8%) e beleza (40,3%).

Em contrapartida, o tema finanças pessoais está entre as áreas menos acompanhadas entre as brasileiras, citado por 9,2% das entrevistadas. Para a economista-chefe do SPC Brasil, Marcela Kawauti, o baixo interesse pelos assuntos relacionados ao orçamento pessoal não é um bom sinal. “É importante buscar instrumentos que facilitem a organização da vida financeira e o melhor controle dos gastos, e as redes sociais podem ser de grande ajuda nesta tarefa”, diz Kawauti. “Ao compartilhar informações sobre esse tema, as consumidoras podem ampliar seu conhecimento sobre finanças pessoais e agir de forma mais consciente, inclusive ensinando outras pessoas como amigos e parentes.”

#### 52,6% das mulheres fazem avaliações de produtos na internet

Se as redes sociais são importantes ferramentas para aprender e debater, também servem para divulgar suas opiniões sobre os produtos comprados. Mais da metade das mulheres entrevistadas (52,6%) costuma fazer avaliações ou comentários na internet sobre os produtos que compra, sendo que três em cada dez (32,4%) o fazem independente de a compra ter sido considerada boa ou ruim; e 20,2% somente quando o produto é ruim. Dentre as mulheres que possuem este hábito, os itens mais avaliados são os celulares (63,0%), as roupas (56,6%), calçados (45,8%), alimentos (32,8%) e equipamentos de TV, DVD e som (30,6%).

“Ao potencializar a circulação e a troca de informações dos produtos comprados, as redes sociais aumentam o poder de escolha das consumidoras, podendo até mesmo provocar mudanças consideráveis em suas preferências e hábitos”, explica Kawauti.


Para a economista, as redes sociais assumem um papel cada vez mais importante no dia-a-dia das brasileiras. “Novos meios online de acesso à informação proporcionam um espaço ilimitado para que as mulheres interajam e compartilhem suas experiências de consumo, funcionando como fóruns de discussão e ajudando na decisão de compra de outras consumidoras”, diz.

Com o aumento do uso de smartphones, os aplicativos também ganham cada vez mais espaço no consumo das mulheres. A pesquisa mostra que cinco em cada dez entrevistadas (49,9%) fazem uso de apps no dia-a-dia, sobretudo as pertencentes às classes C, D e E (45,1%). Dentre as mulheres que utilizam essa ferramenta, os mais populares são aqueles que servem para compra online de roupas e sapatos (24,4%), pedir comida (11,2%), auxiliar a dieta (9,6%) e chamar táxi (9,5%). Já os aplicativos para auxiliar o planejamento do orçamento são citados por 8,8% das entrevistadas.

#### TV ainda é a principal fonte de informação, mas internet ganha força

Considerando os diversos canais que são utilizados para obter informação, o estudo do SPC Brasil e da CNDL revela que a internet já ultrapassou parte dos meios mais tradicionais como jornais e revistas, assumindo um papel cada vez mais importante na vida das brasileiras. Ainda assim, a televisão lidera, sendo mencionada por 68,4% das entrevistadas. Logo após foram citados o Facebook (64,7%), o aplicativo Whatsapp (48,0%) e os portais de notícias (42,0%).

Além de ocupar uma boa posição entre as fontes mais populares para obter informação, o Whatsapp também é citado como o meio de comunicação com maior média de horas dedicadas por dia: são 4,2 horas contra 3,0 horas diárias para o Facebook; 2,9 horas para a TV e 2 horas para o rádio.

Apesar da tecnologia e da variedade de canais informativos, o boca-a-boca com amigos ou familiares foi identificado na pesquisa como o meio mais confiável para as entrevistadas, seguido pela conversa com outros consumidores e pelos jornais. Já as propagandas menos confiáveis na visão das mulheres são aquelas feitas por SMS e por vendedores.

#### 58% acreditam que as propagandas não retratam a mulher real

Ainda que os canais online e tradicionais sejam utilizados pelas mulheres para obter informações, nem sempre elas se sentem plenamente identificadas nesses veículos. Para 58,5% das entrevistadas, as propagandas não refletem suas atitudes e quem elas são.
Entre as brasileiras que acreditam que o perfil de mulher presente na mídia não corresponde à realidade, as principais justificativas são que elas são muito diferentes fisicamente das mulheres reais (59,6%); que elas se sentem incomodadas pela propaganda que as apresenta como objetos sexuais (32,1%); e que as mulheres são sempre mostradas como sendo felizes em famílias perfeitas, e isto não reflete a vida real (29,5%).

A pesquisa então procurou saber quais são os segmentos em que a propaganda mais precisa se adequar para refletir a mulher de hoje e revela que os mais distantes do que as brasileiras pensam ser ideal são o de cerveja, de automóveis, e de moda e beleza. Quando questionadas sobre como gostariam de ser representadas na propaganda, as entrevistadas afirmaram querer ser retratadas como mulheres guerreiras (49,2%), reais e sem o padrão de beleza inatingível dos filmes e da TV (46,6%), dinâmicas (33,0%) e independentes (32,7%).


O estudo do SPC Brasil também investigou a influência do padrão estético das propagandas no consumo das mulheres e mostra que, ainda que 41,8% das entrevistadas acreditem que esse padrão da mídia não influencia no consumo delas, outros 53,7% das entrevistadas admitem comprar roupas e acessórios vistos na mídia para acompanhar as tendências, desde que esses produtos atendam ao gosto pessoal.


Apenas 11,9% das mulheres brasileiras admitem que o padrão estético exerce muita influência nos hábitos de compra, uma vez que sempre compram os produtos ou serviços indicados.

#### Metodologia

O SPC Brasil inicia a série de pesquisas “O Perfil de Consumo das Mulheres Brasileiras”. A amostra abrange 810 mulheres com idade igual ou superior a 18 anos, de todas as classes sociais em todas as regiões brasileiras. A margem de erro é de 3,5 pontos percentuais para um intervalo de confiança a 95%.
