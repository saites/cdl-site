Empresários e contadores participaram em Natal de palestra  sobre Nota Fiscal  Eletrônica. A Convite da FCDL RN e da CDL Natal o auditor Fiscal Marconi Brasil falou sobre os benefícios desse documento, que por enquanto ainda é pouco utilizada aqui no Rio Grande do Norte, mas que já tem empresa operando nesse sistema, como é o caso da Miranda Comunicação, pioneira na implantação do serviço.

O auditor foi bastante questionado pelos presentes que aproveitaram o momento para tirar dúvidas a respeito do tema.
