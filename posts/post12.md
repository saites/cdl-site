
Natal será sede da Assembleia Geral Ordinária da Confederação Nacional dos Jovens empresários (Conaje) em 2017. A decisão foi tomada sexta-feira passada, em Belém, durante a 77ª Assembleia Geral Ordinária da Conaje. Após o anuncio o presidente da CDL Jovem Natal, Rafael Monte comemorou a vitória da concorrência para sediar o evento. A CDL Jovem Natal disputava com os Estados do Maranhão e do Ceará a sede do evento de 2017. "Tivemos uma vitória para a bandeira do empreendedorismo no nosso Estado. Consideramos uma conquista importante para fortalecer o movimento empreendedor jovem local e o trabalho crescente dessa gestão", enfatizou.

A candidatura do RN contou com o apoio da CDL Natal, Federação das Indústrias do Estado do Rio Grande do Norte, SEBRAE/RN, secretarias municipal e estadual de Turismo e Natal Convention Bureau.
A CDL Jovem Natal é o único movimento associativista de jovens empreendedores do Estado, e representa o RN na Confederação Nacional de Jovens Empreendedores. Com atuação e representação em 24 estados brasileiros, a Conaje é uma entidade sem fins lucrativos que atua há 16 anos no fomento ao empreendedorismo, fortalecimento, criação e manutenção de novas empresas – principalmente geridas por jovens -, na articulação e divulgação de práticas capazes de fortalecer a disseminação de novos e sólidos negócios no Brasil.
Assembleias Gerais Ordinárias (AGOs)
Discutir ações, projetos e assuntos estratégicos para os jovens empresários, fortalecer parcerias e debater caminhos para ampliar o ecossistema empreendedor. É com esse objetivo que a Conaje realiza a cada três meses a Assembleia Geral Ordinária (AGO) da instituição. O encontro reúne líderes e representantes de movimentos de jovens empreendedores e empresários das cinco regiões brasileiras.


Crédito da foto:
Arquivo pessoal

Legenda:
Presidente Rafael Monte  e diretor Rafael Bulhões da CDL Jovem Natal defendem a candidatura de Natal para ser sede da AGO em 2017.
