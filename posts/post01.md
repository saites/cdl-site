Empresários e contadores se reuniram hoje na sede da Câmara de
Dirigentes Lojistas de Natal (CDL Natal) com os auditores fiscais do Rio
Grande do Norte para tratar das mudanças nas regras do ICMS, e as
consequências dessas mudanças para os comerciantes. Foram mais de três
horas de conversa, dúvidas operacionais, sobre o SIMPLES nacional, sobre
as cobranças, pagamentos e multas foram esclarecidos. Foi apresentado
também as mudanças provocadas  pela emenda constitucional  n º 87/15 que
trata da partilha provisória do DIFAL.
Os empresários aproveitaram o momento e questionaram a política das
mudanças, falaram das dificuldades do setor com as constantes quedas em
vendas e agora aumento dos impostos. “A situação não está fácil para o
empresário brasileiro. Ano passado foram fechadas 80 mil empresas no
País. As vendas estão ruins e ainda esse aumento os impostos a serem
pagos? A situação só tende a piorar”, desabafou o empresário Orismar de
Almeida, que tem mais de 30 anos de atuação no setor do comércio.

Outro que demonstrou preocupação com as mudanças foi Derneval Sá,
presidente da Associação dos Empresários do Bairro do Alecrim (AEBA).
“Ainda estamos nos informando sobre as mudanças. Esses recolhimentos
antecipados estabelecidos pela GIM cria mais dificuldade na operação de
crédito do lojista, uma vez que agora vamos pagar antecipadamente, para
posteriormente receber o crédito”, comentou ele.

Ao fim do evento os empresários solicitaram uma audiência com o
governador Robinson Farias para tratar do assunto. “Precisamos de prazo
para nos adequarmos, principalmente em relação a GIM,antes o repasse era
feito,agora temos de esperar  60 dias, os empresários não  estavam
preparados para isso, e com certeza essa mudança vai prejudicar o
lojista”, afirmou Afrânio Miranda presidente da Federação das câmaras de
Dirigentes Lojistas do RN (FCDL RN).
O presidente da FCDL/RN  ressaltou ainda que as mudanças foram
publicadas no dia 30 de dezembro  de 2015, e já começaram a valer agora
em primeiro de janeiro, sem prazo para adequação.

Ao fim do evento os presidentes da FCDL RN, Afrânio Miranda e CDL Natal,
Augusto Vaz demonstraram satisfação. “A FCDL RN e a CDL Natal cumpriram
seu papel de proporcionar aos seus associados orientações importantes
sobre as mudanças fiscais e prestar esclarecimentos operacionais”,disse
Afrânio Miranda.

Para o presidente da CDL Natal o evento contribuiu para informar os
lojistas, e ouvir deles a realidade que cada um está passando em  seu
estabelecimentos. “Os lojistas saíram hoje daqui mais informados
tecnicamente sobre as mudanças fiscais. Percebemos que algumas foram
positivas para o empresariado, porém outras preocupam e necessitam de
adequações urgentes por parte dos lojistas, pois um erro pode
comprometer o faturamento da empresa” afirmou Augusto Vaz.
