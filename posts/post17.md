A Liquida Natal completa 15 anos em  2016  e nesta edição busca criar ainda mais oportunidades aos lojistas para vender mais, e assim,  reverter a crise que tem se instalado no setor do comércio. Este ano a CDL Natal e a Start Consultoria decidiram ampliar a premiação para que mais consumidores sejam contemplados. Em 2016  a campanha vai sortear  01 Fiat Toro, 07 TVs Led de 50”, 07 vales compras em supermercado no valor de 500 R$ durante um ano para os consumidores que comprarem nas lojas participantes da Liquida Natal. A cada R$ 30,00 o consumidor ganha um cupom para ser sorteado.

Tradicionalmente a Liquida Natal capacita gratuitamente os funcionários das lojas que aderiram a promoção. Os treinamentos começam nesta quinta-feira 04/8, e são voltados aos profissionais de vendas das empresas. Eles acontecerão todas às quintas-feiras, às18h30, no auditório do SESC – Cidade Alta. Já o último workshop será na quarta-feira, dia 24/08,  no Hotel Holiday Inn.

Quem conversa com os empresários, vendedores e gerentes no primeiro workshop é o especialista Gonçalo Pontes que vai falar  com os participantes sobre a necessidade de sair da inércia para vender mais em momentos de crise.

Dentro da programação da **LIQUIDA NATAL 2016** está prevista a realização de quatro workshops. O objetivo é estimular os participantes, através de uma capacitação motivadora, levando-os a encantar e surpreender os clientes, otimizando as vendas.

#### Confira os dias dos workshops e os palestrantes

### 1º WORKSHOP

Palestrante: Gonçalo Pontes  
Tema: Para vencer a crise saia da inércia  
Dia: 04/08/2016 (Quinta-Feira)  
Hora: 18h30min.  
Local: Auditório SESC / Centro

### 2º WORKSHOP

Palestrante: Leandro Branquinho  
Tema: Dicas poderosas de vendas  
Dia: 11/08/2015 (Quinta-Feira)  
Hora: 18h30min.  
Local: Auditório SESC / Centro

### 3º WORKSHOP

Palestrante: Fred Alecrim  
Tema: Uaugo mais para vender na era do cliente  
Dia: 18/08/2015 (Quinta-Feira)  
Hora: 18h30min.  
Local: Auditório SESC / Centro

### 4º WORKSHOP

Palestrante: Braúlio Bessa  
Tema: Um jeito arretado de empreender  
Dia: 24/08/2015 (Quarta-Feira)  
Hora: 18h30min.  
Local: Auditório Holiday Inn Natal  

*Foto Do treinamento realizado na Liquida de 2015*  
*Crédito - Morais Neto*
