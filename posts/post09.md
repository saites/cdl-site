Esta quinta-feira (21) é dia de Tiradentes e feriado em todo o Brasil. Em Natal, bancos, repartições públicas, escolas, universidades e o comércio de rua vão fechar. Já os shoppings funcionarão, porém com horários diferenciados. A Câmara de Dirigentes Lojistas de Natal (CDL Natal) informa os horários de funcionamento do   comércio neste feriado de 21 de abril.

Comércio de Rua

* Alecrim: Lojas fechadas
* Centro da Cidade: Lojas fechadas
* Zona Norte: Lojas fechadas

Shopping Midway Mall

* Praça de alimentação e Lazer: 11hs às 22hs
* Lojas de Departamento: de 13hs às 21hs
* Demais lojas: de 15hs às 21hs

Natal Shopping

* Praça de Alimentação e Lazer: 11h às 22h
* Lojas âncoras : 12 às 21h
* Mega Lojas:12h às 21h
* Demais Lojas/ Quiosques: 14 às 21h
* Cinema conforme a programação

Praia Shopping

* Praça de Alimentação e Lazer: A partir das 11h
* Lojas e Quiosques: 15 às 21h
* Cinema conforme a programação

Shopping Cidade Jardim

* Praça de Alimentação: A partir das  11h
* Lojas e Quiosques: 15 às 21hs

Shopping Via Direta

* Praça de Alimentação e Lazer: 12h às 22h
* Lojas e Quiosques: Abertura facultativa

Partage Norte Shopping Natal

* Lojas e quiosques: 15h às 21h
* Praça de Alimentação/Lazer: 11h às 22h
* Cinema: Conforme sessões.

Supermercados

* Funcionamento das grandes redes, das 07 às 22hs

Bancos Fechados.
