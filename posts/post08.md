Porque o certificado digital é um documento eletrônico que garante
proteção às transações online e a troca virtual de documentos, mensagens
e dados, com validade jurídica.

Segmentos da economia que utilizam a certificação em suas atividades:

* Receita Federal do Brasil ;
* Área financeira e contábil;
* Poder Judiciário;
* Saúde;
* Educação.

Benefícios da certificação digital:

* Economia de tempo e redução de custos;
* Desburocratização de processos;
* Validade jurídica nos documentos eletrônicos;
* Possibilidade de eliminação de papéis;
* Autenticação na Internet com segurança

O que é necessários para fazer o Certificado Digital
É necessário comparecer à CDL Natal para a validação presencial  e  apresentar os documentos (originais) de acordo com o tipo de certificado  digital escolhido.

Somente originais:

* Dois documentos diferentes de identificação.
* RG, CNH, carteira de Trabalho, Passaporte, CRC, OAB, CREA, CRM (dentro do prazo de validade).
* CPF.
* Comprovante de endereço em nome do titular do certificado – emitido
há, no máximo, três meses.
* Contas de água, luz, telefone fixo, celular ou fatura de cartão de
crédito de banco;
